const SVGO = require('svgo')

module.exports = function (content) {
  const callback = this.async()

  const options = this.resourceQuery.slice(1).split('&')

  //const props = ` ${options.includes('icon') ? 'viewBox="0 0 24 24"' : ''}`

  if (!content.startsWith('<svg')) {
    content = `<svg xmlns="http://www.w3.org/2000/svg">${content}</svg>`
  }
  //content = content.replace('>', props + '>')

  new SVGO({})
    .optimize(content)
    .then(svg => `
      <script>export let props</script>
      ${content.replace('>', ' {...props}>')}
    `)
    .then(component => callback(null, component))
    .catch(callback)
};
